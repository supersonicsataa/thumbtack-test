function save() {
  // Get the text from the textarea.
  var text = document.getElementById("text").value;

  // Save the text to a file.
  var file = new File([text], "allegra.txt");
  var blob = new Blob([file], {type: "text/plain"});
  var link = document.createElement("a");
  link.href = window.URL.createObjectURL(blob);
  link.download = "allegra.txt";
  link.click();
}

if ("serviceWorker" in navigator) {
  window.addEventListener("load", function() {
    navigator.serviceWorker
      .register("/serviceWorker.js")
      .then(res => console.log("service worker registered"))
      .catch(err => console.log("service worker not registered", err))
  })
}